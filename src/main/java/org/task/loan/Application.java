package org.task.loan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 4:58 PM
 */
@ComponentScan
@EnableAutoConfiguration
@ImportResource("classpath:context.xml")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
