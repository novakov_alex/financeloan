package org.task.loan.util;

import java.util.Calendar;

/**
 * @author Alexey Novakov
 *         Date: 6/3/14
 *         Time: 9:25 AM
 */
public class TimeUtils {


    public static long getCurrentTimeInMillis() {
        return System.currentTimeMillis();
    }

    public static int getHours(long applicationTime) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(applicationTime);
        return calendar.get(Calendar.HOUR_OF_DAY);
    }

    public static boolean isHourInInterval(int targetHour, int startIncluding, int endExcluding) {
        return targetHour >= startIncluding && targetHour < endExcluding;
    }
}
