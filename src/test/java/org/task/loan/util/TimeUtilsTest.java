package org.task.loan.util;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author Alexey Novakov
 *         Date: 6/3/14
 *         Time: 2:51 PM
 */
public class TimeUtilsTest {

    @Test
    public void getHours() throws ParseException {
        int expectedHour = 20;
        String target = String.format("%s:29:30", String.valueOf(expectedHour));
        DateFormat df = new SimpleDateFormat("kk:mm:ss");
        Date result = df.parse(target);

        assertTrue("getHours calculation returns wrong value", TimeUtils.getHours(result.getTime()) == expectedHour);
    }

    @Test
    public void getCurrentTime() {
        assertTrue("getCurrentTime returns time in the past",
                System.currentTimeMillis() <= TimeUtils.getCurrentTimeInMillis());
    }

    @Test
    public void checkHourIsInInterval() {
        int target = 8;
        int start = 0;
        int end = 9;
        assertTrue(String.format("target hour = %d was not considered in time interval %d..%d", target, start, end),
                TimeUtils.isHourInInterval(target, start, end));

        target = 9;
        assertFalse(String.format("target hour = %d was considered in time interval %d..%d", target, start, end),
                TimeUtils.isHourInInterval(target, start, end));
    }

}
