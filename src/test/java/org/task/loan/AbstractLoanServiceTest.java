package org.task.loan;

import org.junit.Before;

import java.math.BigDecimal;
import java.util.Random;

/**
 * @author Alexey Novakov
 *         Date: 6/4/14
 *         Time: 8:42 AM
 */
public abstract class AbstractLoanServiceTest {
    protected static final String ERR_MESSAGE_FOR_NEW_LOAN = "application for new loan should not be created, but ";
    protected static final int DEFAULT_LOAN_TERM = 14;
    protected static final int MAX_IP_TOKEN_VALUE = 255;

    protected String ipAddress;
    protected BigDecimal defaultAmount = new BigDecimal(100.50);
    protected int term;
    protected String userRef;

    @Before
    public void setUp() {
        Random random = new Random();
        ipAddress = String.format("test_%d.%d.%d.%d", random.nextInt(MAX_IP_TOKEN_VALUE), random.nextInt(MAX_IP_TOKEN_VALUE),
                random.nextInt(MAX_IP_TOKEN_VALUE), random.nextInt(MAX_IP_TOKEN_VALUE));
        term = random.nextInt(365 - DEFAULT_LOAN_TERM) + DEFAULT_LOAN_TERM;
        userRef = "test_" + System.currentTimeMillis();
    }
}
