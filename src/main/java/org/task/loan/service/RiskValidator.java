package org.task.loan.service;

import java.math.BigDecimal;

import static org.task.loan.util.TimeUtils.getHours;
import static org.task.loan.util.TimeUtils.isHourInInterval;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 4:05 PM
 */

public class RiskValidator {
    public static final BigDecimal MAX_POSSIBLE_AMOUNT = BigDecimal.valueOf(10_000);
    public static final int MAX_APPLICATIONS_PER_DAY = 3;
    public static final int STRICT_PERIOD_START_HOUR = 0;
    public static final int STRICT_PERIOD_END_HOUR = 9;

    private int maxApplicationsPerDay = MAX_APPLICATIONS_PER_DAY;

    public boolean check(BigDecimal amount, long applicationTime, long userApplicationsToday) {

        if (isHourInInterval(
                getHours(applicationTime),
                STRICT_PERIOD_START_HOUR,
                STRICT_PERIOD_END_HOUR)
                && amount.compareTo(MAX_POSSIBLE_AMOUNT) >= 0) {
            return false;
        }

        return userApplicationsToday < getMaxApplicationsPerDay();
    }

    public int getMaxApplicationsPerDay() {
        return maxApplicationsPerDay;
    }

    public void setMaxApplicationsPerDay(int maxApplicationsPerDay) {
        this.maxApplicationsPerDay = maxApplicationsPerDay;
    }

}
