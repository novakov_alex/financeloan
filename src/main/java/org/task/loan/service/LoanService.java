package org.task.loan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.task.loan.dao.UserDao;
import org.task.loan.dao.UserLoanDao;
import org.task.loan.om.User;
import org.task.loan.om.UserLoan;
import org.task.loan.service.om.LoanServiceResult;
import org.task.loan.util.TimeUtils;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 3:11 PM
 */
@Component("loanService")
@Transactional
public class LoanService {
    public static final int RESULT_NOK = 1;
    public static final int RESULT_OK = 0;
    public static final int NO_REFERENCE_ID = -1;
    public static final int LOAN_EXTEND_DAYS_NUMBER = 7;
    public static final Double INTEREST_EXTENSION_FACTOR = 1.5;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserLoanDao userLoanDao;

    @Autowired
    private RiskValidator riskValidator;

    /**
     * Calls method LoanServiceResult applyForLoan(String userRef, BigDecimal amount, int term, String ipAddress, long applyTime)
     * using current system time as applyTime
     */
    public LoanServiceResult applyForLoan(String userRef, BigDecimal amount, int term, String ipAddress) {
        return applyForLoan(userRef, amount, term, ipAddress, TimeUtils.getCurrentTimeInMillis());
    }

    /**
     * User can apply for a new loan by passing amount and term.
     * Loan application risk analysis is performed. Risk is considered too high if:
     * - the attempt to take loan is made after 00:00 with max possible amount.
     * - reached max applications number (RiskValidator.MAX_APPLICATIONS_PER_DAY) per day from a single IP.
     * Loan is issued if there are no risks associated with the application.
     * If so, client gets response status ok - RESULT_OK.
     * However, if risk is surrounding the application, client gets response status error - RESULT_NOK.
     *
     * @param userRef   reference of the user, for example its IP address
     * @param amount    is a amount of money user wants to apply for a new lon
     * @param term      is a number of days for loan due date
     * @param ipAddress of a user's request
     * @param applyTime is a submit date and time user really sent loan request
     * @return object which holds result of the service execution
     * @throws RuntimeException in case userRef is empty or null; in case amount is zero or null
     * @see LoanServiceResult class for more details
     */
    public LoanServiceResult applyForLoan(String userRef, BigDecimal amount, int term, String ipAddress, long applyTime) {
        validateLoanApplication(userRef, amount);

        User user = getUser(userRef);
        long applicationsNum = getApplicationsByIp(user, ipAddress, applyTime);

        if (!riskValidator.check(amount, applyTime, applicationsNum)) {
            return new LoanServiceResult(RESULT_NOK, "you aren't allowed to apply for loan. Try later.", NO_REFERENCE_ID);
        }

        UserLoan userLoan = new UserLoan(user, amount, term, ipAddress, applyTime, getExtendedDueDate(applyTime));
        userLoanDao.create(userLoan);
        user.getLoans().add(userLoan);

        return new LoanServiceResult(RESULT_OK,
                String.format("you have successfully applied for loan {%.2f, %d}", amount, term), userLoan.getUserLoanId());
    }

    private void validateLoanApplication(String userRef, BigDecimal amount) {
        validateUserRef(userRef);

        if (amount == null || amount.compareTo(BigDecimal.valueOf(0)) == 0) {
            throw new RuntimeException("Error: Amount is not provided. It should not be zero");
        }
    }

    /**
     * @param userRef reference of the user, for example its IP address
     * @param loanId  Id of the existing loan to extend for <b>one week</b>,
     *                interest gets increased <b>by a factor of 1.5.</b>
     * @return object which holds result of the service execution
     * @throws RuntimeException in case userRef is empty or null
     * @see LoanServiceResult class for more details
     */

    public LoanServiceResult extendLoan(String userRef, int loanId) {
        validateUserRef(userRef);

        UserLoan userLoan = userLoanDao.findOneByUser(new User(userRef), loanId);

        if (userLoan == null || userLoan.getUserLoanId() == null) {
            return new LoanServiceResult(RESULT_NOK,
                    String.format("there is no loan id = '%d' to extend ", loanId), NO_REFERENCE_ID);
        }

        userLoan.setExtended(true);
        userLoan.setDueDate(getExtendedDueDate(userLoan.getSubmitDate()));
        userLoan.setInterest(userLoan.getInterest() * INTEREST_EXTENSION_FACTOR);
        userLoan = userLoanDao.update(userLoan);

        return new LoanServiceResult(RESULT_OK,
                String.format("you have successfully extended your loan : %d", loanId), userLoan.getUserLoanId());
    }

    /**
     * Returns set of loans for specified userRef.
     *
     * @param userRef reference of the user, for example its IP address
     * @return set of the loans elements using UserLoan entity
     * @throws RuntimeException in case userRef is empty or null
     * @see UserLoan class for more details
     */
    public Set<UserLoan> getLoanHistory(String userRef) {
        validateUserRef(userRef);

        User user = userDao.findOne(userRef);
        return user == null ? new HashSet<UserLoan>() : user.getLoans();
    }

    public void setRiskValidator(RiskValidator riskValidator) {
        this.riskValidator = riskValidator;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void setUserLoanDao(UserLoanDao userLoanDao) {
        this.userLoanDao = userLoanDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }

    public UserLoanDao getUserLoanDao() {
        return userLoanDao;
    }

    public RiskValidator getRiskValidator() {
        return riskValidator;
    }


    private void validateUserRef(String userRef) {
        if (userRef == null || userRef.trim().isEmpty()) {
            throw new RuntimeException("Error: UserRef is not provided.");
        }
    }

    private User getUser(String userRef) {
        User user = userDao.findOne(userRef);

        if (user == null || user.getUserRef() == null) {
            userDao.create(new User(userRef));
        }

        return userDao.findOne(userRef);
    }

    private Long getExtendedDueDate(Long submitDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(submitDate);
        calendar.add(Calendar.DAY_OF_MONTH, LOAN_EXTEND_DAYS_NUMBER);
        return calendar.getTimeInMillis();
    }

    private long getApplicationsByIp(User user, String ipAddress, long applyTime) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(applyTime);

        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        long start = c.getTimeInMillis();

        c.set(Calendar.HOUR_OF_DAY, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        c.set(Calendar.MILLISECOND, 999);
        long end = c.getTimeInMillis();

        return userLoanDao.getLoanCountForPeriod(user, ipAddress, start, end);
    }
}
