package org.task.loan.service;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import static org.junit.Assert.assertFalse;

/**
 * @author Alexey Novakov
 *         Date: 6/3/14
 *         Time: 2:50 PM
 */
public class RiskValidatorTest {
    private RiskValidator riskValidator = new RiskValidator();

    @Test
    public void maxApplicationsForToday() {
        int userApplicationToday = RiskValidator.MAX_APPLICATIONS_PER_DAY;

        assertFalse(String.format("risk validation is wrong applications today for a user is already = %d and " +
                        "limit is %d", userApplicationToday, RiskValidator.MAX_APPLICATIONS_PER_DAY),
                riskValidator.check(new BigDecimal(100), 10000000l, userApplicationToday));
    }

    @Test
    public void maxAmountRiskValidation() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        BigDecimal amount = RiskValidator.MAX_POSSIBLE_AMOUNT.add(BigDecimal.valueOf(1));
        Date applyDate = c.getTime();

        assertFalse(String.format("risk validation is wrong even having more than max possible amount (%f) and " +
                        "hours fo late time = %s ",
                amount.doubleValue(), applyDate), riskValidator.check(amount, applyDate.getTime(), 2));
    }
}
