package org.task.loan.service.om;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 6:22 PM
 */
public class LoanServiceResult {
    private int status;
    private String message;
    private Integer refId;

    public LoanServiceResult(int status, String message, Integer refId) {
        this.status = status;
        this.message = message;
        this.refId = refId;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Integer getRefId() {
        return refId;
    }
}
