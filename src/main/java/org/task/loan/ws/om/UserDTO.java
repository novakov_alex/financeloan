package org.task.loan.ws.om;

import org.task.loan.om.User;

/**
 * @author Alexey Novakov
 */
public class UserDTO {
    private String userRef;

    public static UserDTO convertFrom(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.userRef = user.getUserRef();
        return userDTO;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }
}
