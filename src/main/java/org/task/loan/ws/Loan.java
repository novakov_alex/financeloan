package org.task.loan.ws;

import org.task.loan.ws.om.LoanResponse;
import org.task.loan.ws.om.UserLoanDTO;

import java.util.Set;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 6:04 PM
 */
public interface Loan {

    LoanResponse applyForLoan(String userRef, double amount, int term);

    Set<UserLoanDTO> getLoanHistory(String userRef);

    LoanResponse extendLoan(String userRef, int loanId);
}
