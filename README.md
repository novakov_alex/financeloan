# README #

### What is this repository for? ###

Test task for Java Developer position - API for financial loans. Restful web-service based on embedded servlet container, embedded database.
Technologies: Java EE, JPA-Hibernate, Spring-boot, Maven, Mockito, PowerMock, Jayway.

** Version 1.0. **

### How do I get set up? ###

1. Go to root folder and execute from command line: 
    mvn clean install package
1. then execute:
    java -jar "target/online-loan-service-1.0.jar"

As soon as it is started, below demo requests can be triggered from internet browser:

1. http://localhost:8080/loan/apply?user=1&amount=100&term=65
1. http://localhost:8080/loan/getHistory?user=1
1. http://localhost:8080/loan/extend?user=1&loanId=1
1. http://localhost:8080/loan/getHistory?user=1

E2E Auto-test: mvn -Dtest=LoanServiceUserAcceptanceTestPath test

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner