package org.task.loan.om;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 3:13 PM
 */
@Entity
@Table(name = "user_loan")
public class UserLoan {

    @Id
    @Column(name = "user_loan_id", nullable = false)
    @GeneratedValue
    private Integer userLoanId;

    @ManyToOne(targetEntity = User.class, optional = false)
    @JoinColumn(name = "user_ref")
    private User user;

    @Column(nullable = false)
    private BigDecimal amount;

    @Column(nullable = false)
    private Integer term = 14;

    @Column(name = "ip_address", nullable = false)
    private String ipAddress;

    @Column(nullable = false)
    private Long dueDate;

    private Boolean extended = false;

    @Column(nullable = false)
    private Double interest = 15d;

    @Column(name = "submit_date", nullable = false)
    private Long submitDate;

    public UserLoan() {
    }

    public UserLoan(User user, BigDecimal amount, int term, String ipAddress, Long submitDate, Long dueDate) {
        this.user = user;
        this.amount = amount;
        this.term = term;
        this.ipAddress = ipAddress;
        this.submitDate = submitDate;
        this.dueDate = dueDate;
    }

    public UserLoan(User user, int userLoanId) {
        this.user = user;
        this.userLoanId = userLoanId;
    }

    public Integer getUserLoanId() {
        return userLoanId;
    }

    public void setUserLoanId(Integer userLoanId) {
        this.userLoanId = userLoanId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Integer getTerm() {
        return term;
    }

    public void setTerm(Integer term) {
        this.term = term;
    }

    public Boolean getExtended() {
        return extended;
    }

    public void setExtended(Boolean extended) {
        this.extended = extended;
    }

    public Double getInterest() {
        return interest;
    }

    public void setInterest(Double interest) {
        this.interest = interest;
    }

    public Long getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(Long submitDate) {
        this.submitDate = submitDate;
    }

    @JsonBackReference
    public User getUser() {
        return user;
    }

    public void setUser(User userRef) {
        this.user = userRef;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserLoan)) return false;

        UserLoan userLoan = (UserLoan) o;

        if (amount.compareTo(userLoan.getAmount()) != 0) return false;
        if (extended != null ? !extended.equals(userLoan.extended) : userLoan.extended != null) return false;
        if (!interest.equals(userLoan.interest)) return false;
        if (!submitDate.equals(userLoan.submitDate)) return false;
        if (!term.equals(userLoan.term)) return false;
        if (!userLoanId.equals(userLoan.userLoanId)) return false;
        if (!user.equals(userLoan.user)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (userLoanId != null ? userLoanId.hashCode() : 0);
        result = 31 * result + user.hashCode();
        result = 31 * result + amount.hashCode();
        result = 31 * result + term.hashCode();
        result = 31 * result + (extended != null ? extended.hashCode() : 0);
        result = 31 * result + interest.hashCode();
        result = 31 * result + submitDate.hashCode();
        return result;
    }

    public Long getDueDate() {
        return dueDate;
    }

    public void setDueDate(Long dueDate) {
        this.dueDate = dueDate;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
