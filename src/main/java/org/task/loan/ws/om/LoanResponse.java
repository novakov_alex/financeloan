package org.task.loan.ws.om;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 6:06 PM
 */
public class LoanResponse {
    private int status;
    private String message;

    public LoanResponse(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
