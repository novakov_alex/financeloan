package org.task.loan.om;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 3:29 PM
 */
@Entity
@Table(name = "online_user")
public class User {
    @Id
    @Column(name = "user_ref", unique = true)
    private String userRef;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @OrderBy("submitDate ASC")
    private Set<UserLoan> loans;

    public User() {
        loans = new HashSet<>();
    }

    public User(String userRef) {
        this.userRef = userRef;
    }

    public String getUserRef() {
        return userRef;
    }

    public void setUserRef(String userRef) {
        this.userRef = userRef;
    }

    public Set<UserLoan> getLoans() {
        if (loans == null) {
            loans = new HashSet<>();
        }
        return loans;
    }

    public void setLoans(Set<UserLoan> loans) {
        this.loans = loans;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (loans != null ? !loans.equals(user.loans) : user.loans != null) return false;
        if (!userRef.equals(user.userRef)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return userRef.hashCode();
    }
}
