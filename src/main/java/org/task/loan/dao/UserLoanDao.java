package org.task.loan.dao;

import org.springframework.stereotype.Repository;
import org.task.loan.om.User;
import org.task.loan.om.UserLoan;

import javax.persistence.Query;
import java.util.List;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 10:03 PM
 */
@Repository
public class UserLoanDao extends AbstractJpaDao<UserLoan> {

    public UserLoanDao() {
        super();
        setClazz(UserLoan.class);
    }

    public UserLoan findOneByUser(final User user, final int userLoanId) {
        Query query = entityManager.createQuery("from UserLoan ul where ul.user = ?1 and ul.userLoanId = ?2");
        query.setParameter(1, user);
        query.setParameter(2, userLoanId);
        List<UserLoan> result = query.getResultList();

        return result.size() == 0 ? null : result.get(0);
    }

    public long getLoanCountForPeriod(final User user, String ipAddress, final long start, final long end) {
        Query query = entityManager.createQuery("select count(ul.id) from UserLoan ul where ul.user = ?1 and ul.ipAddress = ?2 " +
                "and ul.submitDate >= ?3 and ul.submitDate < ?4");
        query.setParameter(1, user);
        query.setParameter(2, ipAddress);
        query.setParameter(3, start);
        query.setParameter(4, end);

        return (long) query.getSingleResult();
    }
}
