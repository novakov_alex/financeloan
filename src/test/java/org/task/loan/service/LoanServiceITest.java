package org.task.loan.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.task.loan.AbstractLoanServiceTest;
import org.task.loan.om.UserLoan;
import org.task.loan.service.om.LoanServiceResult;

import javax.transaction.Transactional;
import java.util.Calendar;
import java.util.Set;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 2:50 PM
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context.xml"})
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = false)
@Transactional
public class LoanServiceITest extends AbstractLoanServiceTest {

    @Autowired
    private LoanService loanService;

    @Test
    public void applyForNewLoan() {
        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
        //then
        assertTrue(String.format("applyLoan request is not accepted: %s", result.getMessage()),
                result.getStatus() == LoanService.RESULT_OK);
    }

    @Test
    public void applyForNewLoanOnNextDay() {
        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
        //then
        assertTrue(String.format("applyLoan request is not accepted: %s", result.getMessage()),
                result.getStatus() == LoanService.RESULT_OK);

        //given
        loanService.getRiskValidator().setMaxApplicationsPerDay(1);
        //when
        result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
        //then
        assertTrue(String.format("applyLoan request is accepted: %s", result.getMessage()),
                result.getStatus() == LoanService.RESULT_NOK);

        //given
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, 1);
        //when
        result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress, c.getTimeInMillis());
        //then
        assertTrue(String.format("applyLoan request is not accepted: %s", result.getMessage()),
                result.getStatus() == LoanService.RESULT_OK);
    }

    @Test
    public void extendExistingLoad() {
        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
        //then
        assertTrue(String.format("applyLoan request is not accepted: %s", result.getMessage()),
                result.getStatus() == LoanService.RESULT_OK);
        //then
        UserLoan origLoan = loanService.getUserLoanDao().findOne(result.getRefId());
        Double origInterest = origLoan.getInterest();
        assertNotNull(String.format("There is no new loan {%s, %f, %d}", userRef, defaultAmount.doubleValue(),
                term), origLoan);

        //when
        result = loanService.extendLoan(userRef, result.getRefId());
        //then
        assertTrue("extendLoan fail: " + result.getMessage(), result.getStatus() == LoanService.RESULT_OK);

        //when
        UserLoan userLoan = loanService.getUserLoanDao().findOne(result.getRefId());
        //then
        assertTrue(String.format("loan Id = %d was not really extended", userLoan.getUserLoanId()),
                userLoan.getExtended());
        assertTrue(String.format("Interest for loan Id = %d was not increased properly", result.getRefId()),
                origInterest * LoanService.INTEREST_EXTENSION_FACTOR == userLoan.getInterest());
    }

    @Test
    public void extendUnknownLoad() {
        LoanServiceResult result = loanService.extendLoan(userRef, -1);
        assertTrue("extendLoan fail: " + result.getMessage(), result.getStatus() == LoanService.RESULT_NOK);
    }

    @Test
    public void maxApplicationsPerDay() {
        for (int i = 0; i < loanService.getRiskValidator().getMaxApplicationsPerDay(); i++) {
            //when
            LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
            //then
            assertTrue("extendLoan fail: " + result.getMessage(), result.getStatus() == LoanService.RESULT_OK);
        }

        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
        //then
        assertTrue(ERR_MESSAGE_FOR_NEW_LOAN + result.getMessage(), result.getStatus() == LoanService.RESULT_NOK);

        //given
        String anotherIp = ipAddress + "_max_test";
        //when
        result = loanService.applyForLoan(userRef, defaultAmount, term, anotherIp);
        //then
        assertTrue(String.format("applyForLoan was not accepted: %s", result.getMessage()),
                result.getStatus() == LoanService.RESULT_OK);
    }

    @Test
    public void riskyTimeApplication() throws Exception {
        //given
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, RiskValidator.STRICT_PERIOD_START_HOUR +
                (RiskValidator.STRICT_PERIOD_END_HOUR - RiskValidator.STRICT_PERIOD_START_HOUR) / 2);

        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, RiskValidator.MAX_POSSIBLE_AMOUNT, term, ipAddress,
                c.getTimeInMillis());
        //then
        assertTrue(ERR_MESSAGE_FOR_NEW_LOAN + result.getMessage(), result.getStatus() == LoanService.RESULT_NOK);

        //given
        c.set(Calendar.HOUR_OF_DAY, RiskValidator.STRICT_PERIOD_START_HOUR);// since 0:00 is prohibited
        //when
        result = loanService.applyForLoan(userRef, RiskValidator.MAX_POSSIBLE_AMOUNT, term, ipAddress, c.getTimeInMillis());
        //then
        assertTrue(ERR_MESSAGE_FOR_NEW_LOAN + result.getMessage(), result.getStatus() == LoanService.RESULT_NOK);

        //given
        c.set(Calendar.HOUR_OF_DAY, RiskValidator.STRICT_PERIOD_END_HOUR);// since 9:00 is allowed
        //when
        result = loanService.applyForLoan(userRef, RiskValidator.MAX_POSSIBLE_AMOUNT, term, ipAddress, c.getTimeInMillis());
        //then
        assertTrue(String.format("applyLoan request is not accepted: %s", result.getMessage()),
                result.getStatus() == LoanService.RESULT_OK);

    }

    @Test
    public void getLoanHistoryWithExtendedLoan() {
        //given
        int loansSize = 2;
        loanService.getRiskValidator().setMaxApplicationsPerDay(loansSize);
        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, DEFAULT_LOAN_TERM, ipAddress);
        //then
        assertTrue("insertLoan request #1 is not accepted: " + result.getMessage(),
                result.getStatus() == LoanService.RESULT_OK);

        //when
        result = loanService.applyForLoan(userRef, defaultAmount, 14, ipAddress);
        //then
        assertTrue("insertLoan request #2 is not accepted: " + result.getMessage(),
                result.getStatus() == LoanService.RESULT_OK);

        //when
        result = loanService.extendLoan(userRef, result.getRefId());
        //then
        assertTrue("extendLoan fail: " + result.getMessage(), result.getStatus() == LoanService.RESULT_OK);

        //then
        Set<UserLoan> loanSet = loanService.getLoanHistory(userRef);
        assertTrue(String.format("there are no %d loans in the history for userRef = %s",
                loansSize, userRef), loanSet.size() == loansSize);

        //then
        UserLoan userLoan = loanService.getUserLoanDao().findOne(result.getRefId());
        assertNotNull("just extended loan is not found in a storage", userLoan);
        assertTrue("just extended loan has not been actually extended by service", userLoan.getExtended());
    }

    @Test
    public void getEmptyLoanHistory() {
        //when
        Set<UserLoan> loanSet = loanService.getLoanHistory(ipAddress);
        //then
        assertTrue(String.format("there are %d loans in the history for just created userRef = %s",
                loanSet.size(), ipAddress), loanSet.size() == 0);
    }
}
