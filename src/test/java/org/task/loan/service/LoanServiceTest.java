package org.task.loan.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.task.loan.AbstractLoanServiceTest;
import org.task.loan.dao.UserDao;
import org.task.loan.dao.UserLoanDao;
import org.task.loan.om.User;
import org.task.loan.om.UserLoan;
import org.task.loan.service.om.LoanServiceResult;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;
import static org.task.loan.service.LoanService.*;

/**
 * @author Alexey Novakov
 *         Date: 6/3/14
 *         Time: 2:50 PM
 */
@RunWith(MockitoJUnitRunner.class)
public class LoanServiceTest extends AbstractLoanServiceTest {
    @Mock
    private UserDao userDao;
    @Mock
    private UserLoanDao userLoanDao;
    @Mock
    private RiskValidator riskValidator;
    @InjectMocks
    private LoanService loanService = new LoanService();

    @Before
    public void setup() {
        loanService.setRiskValidator(riskValidator);
        loanService.setUserLoanDao(userLoanDao);
        loanService.setUserDao(userDao);
    }

    @Test
    public void applyForLoanPositiveCase() {
        //given
        when(userDao.findOne(anyString())).thenReturn(new User("user1"));
        when(userLoanDao.getLoanCountForPeriod(any(User.class), anyString(), anyLong(), anyLong())).thenReturn(1L);
        when(riskValidator.check(any(BigDecimal.class), anyLong(), anyInt())).thenReturn(true);

        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
        //then
        assertTrue(
                String.format("new loan was not successful {%s, %f, %d}",
                        userRef,
                        defaultAmount.doubleValue(),
                        term),
                result.getStatus() == RESULT_OK);
        verify(userDao, atLeastOnce()).findOne(anyString());
        verify(userLoanDao, times(1)).getLoanCountForPeriod(any(User.class), anyString(), anyLong(), anyLong());
        verify(userLoanDao, times(1)).create(any(UserLoan.class));
        verify(riskValidator, times(1)).check(any(BigDecimal.class), anyLong(), anyInt());
    }

    @Test(expected = RuntimeException.class)
    public void applyForLoanEmptyUserRef() {
        loanService.applyForLoan(null, defaultAmount, term, ipAddress);
    }

    @Test(expected = RuntimeException.class)
    public void applyForLoanEmptyAmount() {
        loanService.applyForLoan(userRef, null, -1, ipAddress);
    }

    @Test
    public void applyForLoanRiskyCase() {
        //given
        when(userDao.findOne(anyString())).thenReturn(new User());
        when(userLoanDao.getLoanCountForPeriod(any(User.class), anyString(), anyLong(), anyLong())).thenReturn(1L);
        when(riskValidator.check(any(BigDecimal.class), anyLong(), anyInt())).thenReturn(false);
        //when
        LoanServiceResult result = loanService.applyForLoan(userRef, defaultAmount, term, ipAddress);
        //then
        assertTrue("risk validation was not considered", result.getStatus() == RESULT_NOK);
    }

    @Test
    public void extendLoanPositiveCase() {
        //given
        Long originalDueDate = 1000l;
        Double originalInterest = 10d;
        UserLoan userLoan = new UserLoan();
        userLoan.setUserLoanId(1);
        userLoan.setInterest(originalInterest);
        userLoan.setSubmitDate(originalDueDate);
        when(userLoanDao.findOneByUser(any(User.class), anyInt())).thenReturn(userLoan);
        when(userLoanDao.update(any(UserLoan.class))).thenReturn(new UserLoan());

        //when
        LoanServiceResult result = loanService.extendLoan(ipAddress, 1);

        //then
        assertTrue("loan extension was not successful", result.getStatus() == RESULT_OK);
        assertTrue(userLoan.getExtended());
        assertTrue(!userLoan.getDueDate().equals(originalDueDate));
        assertTrue(!userLoan.getInterest().equals(originalInterest));
        verify(userLoanDao, times(1)).findOneByUser(any(User.class), anyInt());
        verify(userLoanDao, times(1)).update(any(UserLoan.class));
    }

    @Test(expected = RuntimeException.class)
    public void extendLoan() {
        loanService.extendLoan(null, 1);
    }

    @Test
    public void extendNonexistentLoan() {
        //given
        when(userLoanDao.findOneByUser(any(User.class), anyInt())).thenReturn(null);
        //when
        LoanServiceResult result = loanService.extendLoan(ipAddress, 1);
        //then
        assertTrue("non-existing loan extension was successful", result.getStatus() == RESULT_NOK);
    }

    @Test
    public void extendedLoanIsNotSaved() {
        //given
        when(userLoanDao.update(any(UserLoan.class))).thenReturn(null);
        //when
        LoanServiceResult result = loanService.extendLoan(ipAddress, 1);
        //then
        assertTrue("loan extension was not saved but service result is successful", result.getStatus() == RESULT_NOK);
    }

    @Test
    public void getHistory() {
        //given
        Set<UserLoan> loans = new HashSet<>();
        int loanSize = 2;

        for (int i = 0; i < loanSize; i++) {
            loans.add(new UserLoan(new User(userRef), defaultAmount, term, ipAddress, (long) i, 1l));
        }

        User user = new User();
        user.setLoans(loans);
        when(userDao.findOne(ipAddress)).thenReturn(user);

        //when
        Set<UserLoan> userLoans = loanService.getLoanHistory(ipAddress);
        //then
        assertTrue(String.format("user does not have %d loans in its history", loanSize),
                userLoans.size() == loanSize);
        //given
        when(userDao.findOne(ipAddress)).thenReturn(null);
        //when
        userLoans = loanService.getLoanHistory(ipAddress);
        //then
        assertTrue("user has some loans in the history", userLoans.size() == 0);
    }
}
