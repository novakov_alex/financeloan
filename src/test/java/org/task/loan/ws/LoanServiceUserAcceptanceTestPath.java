package org.task.loan.ws;

import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author Alexey Novakov
 *         Date: 6/9/14
 *         Time: 5:03 PM
 */
public class LoanServiceUserAcceptanceTestPath {

    private static final int SUCCESS_RESPONSE_STATUS = 0;

    @Test
    public void loanHappyPath() {
        //given
        BigDecimal amount = new BigDecimal(System.currentTimeMillis());
        int term = 30;
        String userRef = "ac_test_" + System.currentTimeMillis();

        //when
        given().
                parameters(
                        "user", userRef,
                        "amount", amount,
                        "term", term
                ).
                expect().
                statusCode(200).
                body("status", equalTo(SUCCESS_RESPONSE_STATUS)).
                when().
                post("/loan/apply/");

        //then
        JsonPath jsonPath = getHistory(userRef);
        Map loan = jsonPath.get("find {e -> e.amount == " + amount + "}");
        assertNotNull("there is no just inserted loan", loan);

        //extend just created loan
        //when
        int loanId = (int) loan.get("userLoanId");
        given().
                parameters(
                        "user", userRef,
                        "loanId", loanId
                ).
                expect().
                statusCode(200).
                body("status", equalTo(SUCCESS_RESPONSE_STATUS)).
                when().
                put("/loan/extend/");

        //then
        jsonPath = getHistory(userRef);
        loan = jsonPath.get("find {e -> e.amount == " + amount + "}");
        assertNotNull(String.format("there is no loan id %d", loanId), loan);
        assertTrue(String.format("loan %d was not actually extended", loanId), (Boolean) loan.get("extended"));
    }

    private JsonPath getHistory(String userRef) {
        Response res = given().
                parameters("user", userRef).
                get("/loan/getHistory");
        return new JsonPath(res.asString());
    }

}
