package org.task.loan.dao;

import org.springframework.stereotype.Repository;
import org.task.loan.om.User;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 11:00 PM
 */
@Repository
public class UserDao extends AbstractJpaDao<User> {

    public UserDao() {
        super();
        setClazz(User.class);
    }

}
