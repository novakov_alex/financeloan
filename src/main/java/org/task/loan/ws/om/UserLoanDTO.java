package org.task.loan.ws.om;

import org.task.loan.om.UserLoan;

/**
 * @author Alexey Novakov
 */
public class UserLoanDTO {
    private int userLoanId;
    private UserDTO user;
    private double amount;
    private int term;
    private String ipAddress;
    private long dueDate;
    private boolean extended;
    private double interest;
    private long submitDate;

    public static UserLoanDTO convertFrom(UserLoan userLoan) {
        UserLoanDTO userLoanDTO = new UserLoanDTO();
        userLoanDTO.userLoanId = userLoan.getUserLoanId();
        userLoanDTO.user = UserDTO.convertFrom(userLoan.getUser());
        userLoanDTO.amount = userLoan.getAmount().doubleValue();
        userLoanDTO.term = userLoan.getTerm();
        userLoanDTO.ipAddress = userLoan.getIpAddress();
        userLoanDTO.dueDate = userLoan.getDueDate();
        userLoanDTO.extended = userLoan.getExtended();
        userLoanDTO.interest = userLoan.getInterest();
        userLoanDTO.submitDate = userLoan.getSubmitDate();
        return userLoanDTO;
    }

    public int getUserLoanId() {
        return userLoanId;
    }

    public void setUserLoanId(int userLoanId) {
        this.userLoanId = userLoanId;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getTerm() {
        return term;
    }

    public void setTerm(int term) {
        this.term = term;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isExtended() {
        return extended;
    }

    public void setExtended(boolean extended) {
        this.extended = extended;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public long getSubmitDate() {
        return submitDate;
    }

    public void setSubmitDate(long submitDate) {
        this.submitDate = submitDate;
    }
}
