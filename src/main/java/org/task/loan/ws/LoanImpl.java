package org.task.loan.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.task.loan.om.UserLoan;
import org.task.loan.service.LoanService;
import org.task.loan.service.om.LoanServiceResult;
import org.task.loan.ws.om.LoanResponse;
import org.task.loan.ws.om.UserLoanDTO;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexey Novakov
 *         Date: 6/1/14
 *         Time: 6:00 PM
 */
@Controller()
@RequestMapping(value = "/loan/**")
class LoanImpl implements Loan {
    @Autowired
    private LoanService loanService;

    @Override
    @RequestMapping(value = "/apply", method = RequestMethod.POST)
    public
    @ResponseBody
    LoanResponse applyForLoan(
            @RequestParam(value = "user", required = true, defaultValue = "") String userRef,
            @RequestParam(value = "amount", required = true, defaultValue = "") double amount,
            @RequestParam(value = "term", required = false, defaultValue = "") int term) {

        LoanServiceResult result = loanService.applyForLoan(userRef, BigDecimal.valueOf(amount), term, getRemoteAddress());
        return new LoanResponse(result.getStatus(), result.getMessage());
    }

    @Override
    @RequestMapping(value = "/extend", method = RequestMethod.PUT)
    public
    @ResponseBody
    LoanResponse extendLoan(
            @RequestParam(value = "user", required = true, defaultValue = "") String userRef,
            @RequestParam(value = "loanId", required = true, defaultValue = "") int loanId) {
        LoanServiceResult result = loanService.extendLoan(userRef, loanId);

        return new LoanResponse(result.getStatus(), result.getMessage());
    }

    @Override
    @RequestMapping(value = "/getHistory", method = RequestMethod.GET)
    public
    @ResponseBody
    Set<UserLoanDTO> getLoanHistory(@RequestParam(value = "user", required = true, defaultValue = "") String userRef) {
        Set<UserLoanDTO> loanDTOs = new HashSet<>();

        for (UserLoan userLoan : loanService.getLoanHistory(userRef)) {
            loanDTOs.add(UserLoanDTO.convertFrom(userLoan));
        }

        return loanDTOs;
    }

    private String getRemoteAddress() {
        return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getRemoteAddr();
    }
}
